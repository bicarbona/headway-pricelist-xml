<?php

class print_pricelist_xmlBlock extends HeadwayBlockAPI {

    public $id = 'print_pricelist_xml';
    public $name = 'Headway Print Pricelist XML';
    public $options_class = 'print_pricelist_xmlBlockOptions';
    public $description = 'Print Pricelist XML';
   
	function enqueue_action($block_id) {

		/* CSS */
	//	wp_enqueue_style('headway-pin-board', plugin_dir_url(__FILE__) . '/css/pin-board.css');		

		/* JS */
	//	wp_enqueue_script('headway-pin-board', plugin_dir_url(__FILE__) . '/js/pin-board.js', array('jquery'));		

	}
		
    public function content($block) {
	global $doc;     // if outside the loop
	global $root;     // if outside the loop
		 

		 ?>
<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="get">
<input type="hidden" name="cennik-xml" value="yes">
<input type="submit" value="XML">	
</form>
		 <?php

		   function get_custom_terms($taxonomies, $args){

		/**
		TODO:
		pridat moznost / show /hide label

		**/

		  //  $args = array('orderby'=>'asc','hide_empty'=>true);
		    // wp_get_object_terms( $post->ID, 'portfolio-skills', array( 'fields' => 'names' ) );
		   // $custom_terms = get_terms(array($taxonomies), $args);
		   $custom_terms = get_the_terms($post->ID, $taxonomies);

		    foreach($custom_terms as $term){
		    // echo 'Term slug: ' . $term->slug . ' Term Name: ' . $term->name;
		    
		    // Zobrazim label taxonomie
		    $the_tax = get_taxonomy($taxonomies);
		       // echo $the_tax->labels->name;

		        // zobrazim term / vyraz / polozku
		        return $term->name;
		    // ak ma popis tak ho zobrazim
		        // if ($term->description) {
		        //   //  echo ' <strong> skratka  </strong> ' . $term->description;
		        // }
		  //  echo  ' <br />';
		    }
		}

/**
Taxonomy
**/
			function norm_spec_char( $str ) {
				$unwanted_array = array(
						"&amp;" 	=>	"&" ,     #ampersand 
						"&#8217;"	=>	"’",
						"&deg;"		=>	"°",
						"&#8211;"	=> "–",  //–  //—
						"&nbsp;"	=> " "
				);

				$str = strtr( $str, $unwanted_array );
			    return $str;
			}

function food_menu_block($in_category, $not_in_category ){
	$category_description = trim( preg_replace( '/\s+/', ' ', category_description($in_category) ) );  
	global $doc;     // if outside the loop
	global $root;     // if outside the loop
	global $polozka;     // if outside the loop

		// $title = $doc->createElement('title');
		// $title = $root->appendChild($title);

		// $text = $doc->createTextNode('xx'.$taxonomy->name);
		// $text = $title->appendChild($text);
	$args = array(
	'post_type' => 'cennik',
	'orderby' => 'druh',
	'order'=> 'ASC',
	'post_status' => 'publish',
	'posts_per_page' => -1,

    'tax_query' => array(                     //(array) - use taxonomy parameters (available with Version 3.1).
  //  'relation' => 'AND',                      //(string) - Possible values are 'AND' or 'OR' and is the equivalent of ruuning a JOIN for each taxonomy
      array(
        'taxonomy' => 'druh',                //(string) - Taxonomy.
        'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug')
        // 'terms' => array( 'kava', 'vino', 'pivo', 'domace-limonady', 'voda' , 'bruschetta', 'caj', 'kofola','predjedla', 'salaty', 'sunkove-variacie', 'sladke-2'),    //(int/string/array) - Taxonomy term(s).
        'terms' =>  $in_category,
        'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
        'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
      )
    ),
 );

	$taxonomy = get_term_by( 'slug', $in_category, 'druh');

/**
XML Category Title
**/
		$title = $doc->createElement('category-title');
		$title = $root->appendChild($title);
		$text = $doc->createTextNode($taxonomy->name);

		//$text = $doc->createTextNode($taxonomy->name."\n");
		$text = $title->appendChild($text);

/**
WP query
**/
	$query = null;
	$query = new WP_Query($args);

	while ($query->have_posts()) : $query->the_post(); 

/**
XML
**/
	$polozka = $doc->createElement('polozka');
	$polozka = $root->appendChild($polozka);
/**

**/
	$objem_1 = get_field_object('objem_1', $thePostID);
	$objem_2 = get_field_object('objem_2', $thePostID);

	$cena_1 = get_field_object('cena_1', $thePostID);
	$cena_2 = get_field_object('cena_2', $thePostID);
	$cena_3 = get_field_object('cena_3', $thePostID);

	$hmotnost_1 = get_field_object('hmotnost_1', $thePostID);
	$hmotnost_2 = get_field_object('hmotnost_2', $thePostID);
	$hmotnost_vlastne = get_field_object('hmotnost_vlastne', $thePostID);

			/**
			Objem
			**/

				if (get_field('objem_1') && get_field('objem_2') ) {
					$objem_hmotnost_text = $doc->createTextNode("\t".$objem_1['value'] .' / '. $objem_2['value'] .' l'."\t");
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
				} 

				else if (get_field('objem_1') && get_field('objem_2') =='' ) {
					$objem_hmotnost_text = $doc->createTextNode("\t".$objem_1['value'] .' l'."\t");
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
				
				} 

				else {
					$objem_hmotnost_text = $doc->createTextNode("\t");
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
				}

			/** 
			hmotnost
			**/



			if (get_field('osoby')){

				if( in_array( '1', get_field('osoby'))  && in_array( '2', get_field('osoby'))){

					$objem_hmotnost_text = $doc->createTextNode("\t".'1 os. / 2 os.'."\t");
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
				//	echo "oleee";

				} else

				if( in_array( '1', get_field('osoby') ) ){
					$objem_hmotnost_text = $doc->createTextNode("\t".'1 os.');
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);

				} else 
				
				if( in_array( '2', get_field('osoby') ) ){
					$objem_hmotnost_text = $doc->createTextNode("\t".'2 os.');
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
		
				}
			} else{


				if (get_field('hmotnost_1') && get_field('hmotnost_2')) {

					$objem_hmotnost_text = $doc->createTextNode("\t".$hmotnost_1['value'] .' / '. $hmotnost_2['value'] .' g'."\t");
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
					//echo '<objem-hmotnost>'."\t". $hmotnost_1['value'] .' g'."</objem-hmotnost>";
				}

				if (get_field('hmotnost_2') && get_field('hmotnost_1')=='') {

					$objem_hmotnost_text = $doc->createTextNode("\t".$hmotnost_1['value'] .' / '. $hmotnost_2['value'] .' g'."\t");
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
					//echo '<objem-hmotnost>'."\t". $hmotnost_2['value'] .' g'."</objem-hmotnost>";
				}

				if (get_field('hmotnost_vlastne')) {

					$objem_hmotnost_text = $doc->createTextNode($hmotnost_vlastne['value'] .' g');
					$objem_hmotnost_text = $polozka->appendChild($objem_hmotnost_text);
				}
			}

				/**
				Title
				**/
					if(get_custom_terms('rocnik')){
						$rocnik = ' '.get_custom_terms('rocnik');
					}

					$title_text = $doc->createTextNode(norm_spec_char(get_the_title()) .$rocnik);
					$title_text = $polozka->appendChild($title_text);

			/**
			Cena
			**/
			if ((get_field('cena_1') != 'ZADARMO') && (get_field('cena_2') != 'ZADARMO') && (get_field('cena_3') != 'ZADARMO')){
			//if((get_field('cena_1') || get_field('cena_2') || get_field('cena_3') ) !== 'ZADARMO'){

				if ( get_field('cena_1') && get_field('cena_2') && get_field('cena_3') ) {

					$cena_text = $doc->createTextNode("\t". $cena_1['value'] .' / '. $cena_2['value'] .' / '. $cena_3['value'] .' €'."\xE2\x80\xA8");
					$cena_text = $polozka->appendChild($cena_text);

				} else if (get_field('cena_1') && get_field('cena_2')) {

					$cena_text = $doc->createTextNode("\t". $cena_1['value'] .' / '. $cena_2['value'] .' €'."\xE2\x80\xA8");
					$cena_text = $polozka->appendChild($cena_text);

				} else if (get_field('cena_1')) {
					
					$cena_text = $doc->createTextNode("\t". $cena_1['value'] .' €'."\xE2\x80\xA8");
					$cena_text = $polozka->appendChild($cena_text);

				}
			/**
			ZADARMO
			**/
			}else {
					$cena_text = $doc->createTextNode('ZADARMO');
					$cena_text = $polozka->appendChild($cena_text);
			}

			/**
			Popis k vinam
			**/
					if(get_custom_terms('vinar')){

						$vinar = $doc->createElement('vino-popis');
						$vinar = $polozka->appendChild($vinar);

						$vinar_text = $doc->createTextNode(norm_spec_char(get_custom_terms('vinar')));
						$vinar_text = $vinar->appendChild($vinar_text);
					}

					if(get_custom_terms('oblast')){

						$vinar_text = $doc->createTextNode(', '. get_custom_terms('oblast'));
						$vinar_text = $vinar->appendChild($vinar_text);

					}

					if(get_custom_terms('obsah_cukru')){
					
						$vinar_text = $doc->createTextNode(', '. get_custom_terms('obsah_cukru'));
						$vinar_text = $vinar->appendChild($vinar_text);

					}

					if(get_custom_terms('privlastok')){

					$vinar_text = $doc->createTextNode(', '. get_custom_terms('privlastok'));
					$vinar_text = $vinar->appendChild($vinar_text);
					// echo ', '.norm_spec_char( get_custom_terms('privlastok'));

					}

		// if(get_the_excerpt() !='') echo "\n\t".'<popis>'. get_the_excerpt(). '</popis>';
			//echo "\n\t".'<popis>'. get_the_excerpt(). '</popis>';
	//	echo '</polozka>'."\n";

		/**
		PODMIENIT SEPARATOR
		**/
		//echo '<separator>'."\n".'</separator>';
		endwhile;
}
 
// Vytvorim XML dokukemnt
$doc = new DOMDocument('1.0');
// Nastavim pekne formatovanie
$doc->formatOutput = true;
$doc->encoding = 'UTF-8';
// vytvorim ROOT element
$root = $doc->createElement('root');
$root = $doc->appendChild($root);

/**

**/	
	food_menu_block('predjedla',0); 			// predjedla
	food_menu_block('bruschetta',0); 			// bruschetta
	food_menu_block('sunkove-variacie',0); 		// sunkove-variacie
	food_menu_block('salaty',0); 				// salaty
	food_menu_block('sladke-2',0); 				// sladke

	food_menu_block('tankove-vino',0); 			// tankove vino
	food_menu_block('biele-2',0); 				// biele

	food_menu_block('rose-2',0); 				// ruzove
	food_menu_block('cervene-2',0); 			// ruzove
	food_menu_block('sangria',0); 				// sangria

	food_menu_block('kava',0); 					// kava
	food_menu_block('caj',0); 					// caj

		//echo '<pageBreak>&#x2028;</pageBreak>';
	food_menu_block('domace-limonady',0); 		// limonady
	food_menu_block('voda',0); 					// voda
	food_menu_block('pivo',0); 					// pivo

//OFF	
	// $pageBreak = $doc->createElement('pageBreak');
	// $pageBreak = $doc->appendChild($pageBreak);

	food_menu_block('salaty',0); 	// Kava
	
	//echo '<CharacterStyleRange AppliedCharacterStyle="CharacterStyle/$ID/[No character style]" ParagraphBreakType="NextPage"><Br/> </CharacterStyleRange>';
	//echo '<pageBreak>&#x2028;</pageBreak>';
	/* echo '<CharacterStyleRange AppliedCharacterStyle="CharacterStyle/$ID/[No character style]" PageNumberType="NextPageNumber"><Content><?ACE 18?></Content> </CharacterStyleRange>';*/
 
$upload_base_dir = wp_upload_dir();
$upload_dir = $upload_base_dir['basedir']."/cennik/"; 

if (wp_mkdir_p($upload_dir)) {
  echo 'It worked!';
}

$xml_file = $upload_base_dir['basedir']."/cennik/cennik.xml"; 


// /Volumes/WORK/Server/viecha/wp-content/plugins/headway-print-pricelist/includes
///Users/gentleman/Documents/cennik.xml
 //echo '<xmp>'. 'Wrote: ' . $doc->save("/Volumes/WORK/Server/viecha/wp-content/plugins/headway-pricelist-migrate/includes/cennik.xml") . ' bytes'.'</xmp>'; // Wrote: 72 bytes

?>

<?php

		// if($_GET['cennik-xml']){
		// 	//echo 'trala';
		// 	 echo '<xmp>'. 'Wrote: ' . $doc->save($xml_file) . ' bytes'.'</xmp>'; // Wrote: 72 bytes

		// }
			 echo '<xmp>'. 'Wrote: ' . $doc->save($xml_file) . ' bytes'.'</xmp>'; // Wrote: 72 bytes


    }
}