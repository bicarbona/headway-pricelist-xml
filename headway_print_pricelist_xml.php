<?php
/**
 * Plugin Name: Headway Print Pricelist XML
 * Plugin URI:  https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-pricelist-xml
 * Description: Print Pricelist XML
 * Version:     0.1.3
 * Author:      Bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: print_pricelist_xml
 * Domain Path: /languages
 * Bitbucket Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-pricelist-xml
 * Bitbucket Branch: master
 */


define('PRINT_PRICELIST_XML_BLOCK_VERSION', '0.1.0');

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'print_pricelist_xml_register');
function print_pricelist_xml_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('print_pricelist_xmlBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'print_pricelist_xml_prevent_404');
function print_pricelist_xml_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'print_pricelist_xml_redirect');
function print_pricelist_xml_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}